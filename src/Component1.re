/* State declaration */
type state = {
  lon: float,
  now: float,
};

/* Action declaration */
type action =
  | UpdateLon(string);

let component = ReasonReact.reducerComponent("Page");

let padStart = (~len, ~char, str) => {
  let charsToAdd = len - String.length(str);
  let padding = charsToAdd > 0 ? String.make(charsToAdd, char) : "";
  padding ++ str;
};

let formatDate = (~year, ~month, ~date, ~hour, ~minute) => {
  let strYear = string_of_int(int_of_float(year));
  let strMonth = padStart(~len=2, ~char='0', string_of_int(1 + int_of_float(month)));
  let strDate = padStart(~len=2, ~char='0', string_of_int(int_of_float(date)));
  let strHour = padStart(~len=2, ~char='0', string_of_int(int_of_float(hour)));
  let strMinute = padStart(~len=2, ~char='0', string_of_int(int_of_float(minute)));
  {j|$(strYear)-$(strMonth)-$(strDate)T$(strHour):$(strMinute)|j};
};

let convertLonToUtcOffsetInMs = (~lon) => lon *. 1000.0 *. 60.0 *. 60.0 *. 12.0 /. 180.0;

let lmt = (~lon, ~date) => {
  let offset = convertLonToUtcOffsetInMs(~lon);
  let localTime = offset +. Js.Date.valueOf(date);
  let localDate = Js.Date.fromFloat(localTime);
  formatDate(
    ~year=Js.Date.getUTCFullYear(localDate),
    ~month=Js.Date.getUTCMonth(localDate),
    ~date=Js.Date.getUTCDate(localDate),
    ~hour=Js.Date.getUTCHours(localDate),
    ~minute=Js.Date.getUTCMinutes(localDate)
  );
};

let make = (_children) => {
  ...component,

  initialState: () => {
    lon: 174.87,
    now: Js.Date.now(),
  },

  /* State transitions */
  reducer: (action, state) =>
    switch (action) {
    | UpdateLon(lon) => ReasonReact.Update({...state, lon: float_of_string(lon)})
    },

  render: self => {
    let now = Js.Date.fromFloat(self.state.now);
    let lon = self.state.lon;
    let nowUtc = formatDate(
      ~year=Js.Date.getUTCFullYear(now),
      ~month=Js.Date.getUTCMonth(now),
      ~date=Js.Date.getUTCDate(now),
      ~hour=Js.Date.getUTCHours(now),
      ~minute=Js.Date.getUTCMinutes(now)
    );
    let nowLocal = formatDate(
      ~year=Js.Date.getFullYear(now),
      ~month=Js.Date.getMonth(now),
      ~date=Js.Date.getDate(now),
      ~hour=Js.Date.getHours(now),
      ~minute=Js.Date.getMinutes(now)
    );
    let nowLmt = lmt(~lon=174.87, ~date=now);
    <div>
      <input
        value=(string_of_float(lon))
        onChange=(_event => self.send(UpdateLon(ReactEvent.Form.target(_event)##value)))
      />
      <table>
        <tbody>
          <tr>
            <td>(ReasonReact.string("UTC:"))</td>
            <td>(ReasonReact.string(nowUtc))</td>
          </tr>
          <tr>
            <td>(ReasonReact.string("TZ time:"))</td>
            <td>(ReasonReact.string(nowLocal))</td>
          </tr>
          <tr>
            <td>(ReasonReact.string("LMT:"))</td>
            <td>(ReasonReact.string(nowLmt))</td>
          </tr>
        </tbody>
      </table>
    </div>;
  },
};
